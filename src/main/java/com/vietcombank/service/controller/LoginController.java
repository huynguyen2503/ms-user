package com.vietcombank.service.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vietcombank.service.service.JwtTokenService;
import com.vietcombank.service.service.UserService;

import lombok.extern.slf4j.Slf4j;



@RestController
@RequestMapping("/restapi")
@Slf4j
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenService jwtTokenService;
	
	@Autowired
	private UserService userService;
	

	@PostMapping("/login/default")
	public ResponseEntity<?> loginDefault(@RequestParam("username") String username, @RequestParam("password") String password) {
	    String key = "Login";
	    log.info("Login | ", key + " with username: " + username);

	    try {
	        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	        String token = jwtTokenService.createToken(username, Collections.emptyList()); // No roles

	        return ResponseEntity.ok(token);
	    } catch (AuthenticationException e) {
	        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); // Unauthorized status if authentication fails
	    }
	}
}
