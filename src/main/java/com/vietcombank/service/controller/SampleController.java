//package com.vietcombank.service.controller;
//
//import com.vietcombank.service.service.SampleTableService;
//import io.swagger.v3.oas.annotations.Operation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@Slf4j
//@RestController
//@RequestMapping("/restapi")
//public class SampleController {
//    @Autowired
//    private SampleTableService sampleTableService;
//
//    @GetMapping(value = "/v1/products", produces = "application/json")
//    @Operation(summary = "Retrieve All Product")
//    public ResponseEntity<?> retrieveProducts() {
//        log.info("retrieve all products");
//        return ResponseEntity.ok(sampleTableService.retrieveAll());
//    }
//}
