package com.vietcombank.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vietcombank.service.entity.User;
import com.vietcombank.service.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class UserController { 
	
	@Autowired
	private UserService userService;
	
	
    @GetMapping(value = "/v1/users")
    @Operation(summary = "Get all users")
    public ResponseEntity<?> getAllUsers() {
        log.info("Get all users");
        return ResponseEntity.ok(userService.getListUsers());
    }
    
    @PostMapping(value = "/createAccountUser")
    @Operation(summary = "Create account user")
    public ResponseEntity<?> createAccountUser(@RequestBody User user) {
        log.info("Create account user");
        return ResponseEntity.ok(userService.createAccountUser(user));
    }
    
    @GetMapping(value = "/v1/{id}")
    @Operation(summary = "Get user by id")
    public ResponseEntity<?> getUserById(@PathVariable("id") int id) {
        log.info("Get user by id");
        return ResponseEntity.ok(userService.getUserById(id));
    }
    
    @DeleteMapping(value = "/v1/delete/{id}")
    @Operation(summary = "Delete user by id")
    public ResponseEntity<?> deleteUserById(@PathVariable("id") int id) {
        log.info("Delete user by id");
        userService.deleteUserById(id);
        return ResponseEntity.ok("Delete user successfully");
    }
    
    @PutMapping(value = "/v1/update")
    @Operation(summary = "Update user")
    public ResponseEntity<?> updateUser(@RequestBody User user) {
        log.info("Update user");
        return ResponseEntity.ok(userService.updateAccountUser(user));
    }

}
