package com.vietcombank.service.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vietcombank.service.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	Optional<User> findByUsername(String username);
}
