package com.vietcombank.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.vietcombank.service.entity.User;
import com.vietcombank.service.repository.UserRepository;

import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public User createAccountUser(User user) {
	    String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
	    user.setPassword(encodedPassword);
		return userRepository.save(user);
	}
	
	public User getUserById(int id) {
		return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found user"));
	}
	
	public List<User> getListUsers() {
		return userRepository.findAll();
	}
	
	public User updateAccountUser(User user) {
		User existingUser = userRepository.findById(user.getId()).orElseThrow(() -> new EntityNotFoundException("Not found user"));
		existingUser.setEmail(user.getEmail());
		existingUser.setPhone(user.getPhone());
		existingUser.setPassword(user.getPassword());
		existingUser.setCard(user.getCard());

	    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	    existingUser.setPassword(passwordEncoder.encode(user.getPassword())); // Mã hóa mật khẩu mới
	    
		return userRepository.save(existingUser);
	}
	
	public void deleteUserById(int id) {
		 userRepository.deleteById(id);
	}
	
}
