package com.vietcombank.service.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JwtTokenFilter extends OncePerRequestFilter {
	
	@Autowired
	private JwtTokenService jwtTokenService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String bearerToken = request.getHeader("Authorization");
		log.info(bearerToken);
		
		if(bearerToken != null && bearerToken.startsWith("Bearer ")) {
			
			// Bỏ chữ Bearer
			String token = bearerToken.substring(7);
			if(!token.isEmpty()) {
				String username = jwtTokenService.getUsername(token);
			if(username != null) {
				Authentication auth = jwtTokenService.getAuthentication(username);
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}
	}

	filterChain.doFilter(request, response);
	}

}
