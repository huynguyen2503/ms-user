package com.vietcombank.service.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtTokenService {
	
	private final UserDetailsService userDetailsService;

	@Value("${jwt.secret:123456}")
	private String secretKey;
	
	private long validityInMilliseconds = 900000;
	
	public String createToken(String username, List<String> roles) {
		
		Claims claims = Jwts.claims()
				.setSubject(username);
		claims.put("roles", roles);
		Date now = new Date();
		Date expriedTime = new Date(now.getTime() + validityInMilliseconds);
		String accessToken = Jwts.builder().setClaims(claims)
				.setIssuedAt(now)
				.setExpiration(expriedTime)
				.signWith(SignatureAlgorithm.HS256, secretKey)
				.compact();
		return accessToken;
	}
	
	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Invalid token!");
			return false;
		}
	}
	
	// Lấy username từ JWT. 
	public String getUsername(String token) {
		try {
			// Lấy ra subject là username
			return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token)
					.getBody().getSubject();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "Can't get username. Please try again!";
		}
	}
	
	// Tạo 1 đối tượng Authentication từ thông tin username của người dùng.
	public Authentication getAuthentication(String username) {
		try {
			// Lấy thông tin người dùng từ DB
			// Sau đó hàm sử dụng thông tin người dùng để tạo UsernamePasswordAuthenticationToken
			// Để xác thực người dùng
			UserDetails userDetails = userDetailsService.loadUserByUsername(username);
			return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
}
