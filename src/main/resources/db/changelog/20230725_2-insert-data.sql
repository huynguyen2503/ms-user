--liquibase formatted sql
--changeset microservice_class:20230725_2

INSERT INTO users
(username, password, email, phone, creation_date, card)
VALUES ('ninhmanhtien123', 'cgv123456', 'tiennm@gmail.com', '123456789', '2023-07-25 11:34:53.629', '1234-5678-9012-3456'),
       ('nguyenquanghuy456', 'cgv123456', 'huynq@gmail.com', '987654321', '2023-07-25 11:35:53.629', '5678-9012-3456-7890');