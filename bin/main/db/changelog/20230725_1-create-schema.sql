--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users
(
    id serial PRIMARY KEY,
    username varchar(255),
    password varchar(255),
    email varchar(255),
    phone varchar(255),
    creation_date timestamp NOT NULL,
    card varchar(255)
);